#include <iostream>
#include <windows.h>

int main()
{
    // Initialize elements of Array
    const int ArraySize = 11;
    int Array[ArraySize][ArraySize];

    for (int i = 0; i < ArraySize; i++)
    {
        for (int j = 0; j < ArraySize; j++)
        {
            Array[i][j] = i + j;
        }
    }

    // Print elements of Array
    for (int i = 0; i < ArraySize; i++)
    {
        for (int j = 0; j < ArraySize; j++)
        {
            std:: cout << Array[i][j];

            // Condition for readable output of array elements
            if (Array[i][j] / 10 != 0)
            {
                std::cout << " ";
            }
            else
            {
                std::cout << "  ";
            }
        }

        std::cout << "\n";
    }
    std::cout << "\n";

    // Date request
    SYSTEMTIME st;
    GetSystemTime(&st);
    int Day = st.wDay; 
    std::cout << "Today is the " << Day << "th day of the month\n";;
    //

    // Numbers summation of Array line
    int SumElements = 0;

    for (int j = 0; j < ArraySize; j++)
    {
        SumElements += Array[Day % ArraySize - 1][j];
    }

    std::cout << "Sum of array line '" << Day % ArraySize << "' = " << SumElements << "\n";

}
